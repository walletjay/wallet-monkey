# Wallet Monkey

Wallet Monkey is a personal finance platform specializing in credit card data specific to banks and credit unions in the United States. 

## Name
Wallet Monkey Data & API

## Description
Wallet Monkey offers consumer data points on credit cards and credit bureaus inside the United States. We are working to provide api access to these data sets.

## Visuals
Data examples and guides can be found at [Wallet Monkey](https://walletmonkey.io)
- [Master Credit Card Lender List](https://walletmonkey.io/master-credit-card-lender-list/)
- [Definitive Guide To American Express](https://walletmonkey.io/definitive-guide-to-american-express-credit-cards/)
- [Definitive Guide To Navy Federal Credit Union](https://walletmonkey.io/definitive-guide-to-navy-federal-credit-union/)
- [Definitive Guide to Capital One Credit Cards](https://walletmonkey.io/definitive-guide-to-capital-one-credit-cards)

## Support
contact support@walletmonkey.io

## Roadmap
Check back for roadmap updates this year for features and relase dates.

## License
Paid licensing offerd

## Project status
Project isn't yet completed, we are in beta
